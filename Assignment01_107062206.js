const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');
var historyimage = new Array();
var redoimage = new Array();


var redo, undo, download;
var index = 1, max_index = 1;
var selected = "none";
let filled = false;


const downEvent = 'mousedown';
const moveEvent = 'mousemove';
const upEvent = 'mouseup';
let isMouseActive = false;

let x1, y1, x2, y2, x3, y3, x4, y4;

function up() {
    ctx.globalCompositeOperation = 'source-over';
}
function down() {
    ctx.globalCompositeOperation = 'destination-over';
}
function color () {
    ctx.globalCompositeOperation = 'destination-over';
    historyimage[index] = ctx.getImageData(0, 0, canvas.width, canvas.height);
    index++;
    max_index = index;
    ctx.beginPath();
    ctx.fillStyle = document.getElementById("color").value;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.globalCompositeOperation = 'source-over';
}
function text () {
    selected = "text";
    canvas.style.cursor = `url('./image/text.svg'), auto`;
}
function reset () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

document.getElementById('upload').onchange = function(e) {
    var img = new Image();
    img.onload = draw;
    img.onerror = failed;
    img.src = URL.createObjectURL(this.files[0]);
};
function draw() {
    var img_w,img_h;
    img_w = this.width;
    img_h = this.height;
    if(img_h > canvas.height) {
        img_h = canvas.height;
    }
    if(img_w > canvas.width) {
        img_w = canvas.width;
    }
    ctx.drawImage(this, (canvas.width-img_w)/2, (canvas.height-img_h)/2);
}
function failed() {
    console.error("The provided file couldn't be loaded as an Image media");
}

function brush() {
    selected = "brush";
    canvas.style.cursor = `url('./image/pencil.svg'), auto`;
}
function eraser() {
    selected = "eraser";
    canvas.style.cursor = `url('./image/eraser.svg'), auto`;
}
function special() {
    selected = "special";
    
}

function rect() {
    selected = "rect";
    filled = false;
    canvas.style.cursor = `url('./image/rectangle.svg'), auto`;
}
function rect_fill() {
    selected = "rect";
    filled = true;
    canvas.style.cursor = `url('./image/rectangle.svg'), auto`;
}
function trian() {
    selected = "trian";
    filled = false;
    canvas.style.cursor = `url('./image/triangle.svg'), auto`;
}
function trian_fill() {
    selected = "trian";
    filled = true;
    canvas.style.cursor = `url('./image/triangle.svg'), auto`;
}
function circle() {
    selected = "circle";
    filled = false;
    canvas.style.cursor = `url('./image/circle.svg'), auto`;
}
function circle_fill() {
    selected = "circle";
    filled = true;
    canvas.style.cursor = `url('./image/circle.svg'), auto`;
}

undo = function() {
    if(index > 1) {
        historyimage[index] = ctx.getImageData(0, 0, canvas.width, canvas.height);
        index--;
        ctx.putImageData(historyimage[index],0,0);
    }else {
        return;
    }

}
redo = function() {  
    if(index < max_index) {
        index++;
        ctx.putImageData(historyimage[index],0,0);
    }else {
        return;
    }
}

canvas.addEventListener(downEvent, function (e){
    historyimage[index] = ctx.getImageData(0, 0, canvas.width, canvas.height);
    index++;
    max_index = index;
    if(selected == "brush") {
        isMouseActive = true;
        x1 = e.offsetX;
        y1 = e.offsetY;
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("size").value;
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';
        
    }else if(selected == "eraser") {
        isMouseActive = true  ;
        x3 = e.offsetX; 
        y3 = e.offsetY;
        
    }else if(selected == "rect") {
        isMouseActive = true;
        x1 = e.offsetX;
        y1 = e.offsetY;
        ctx.fillStyle = document.getElementById("color").value;
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("size").value;
        ctx.lineJoin = 'miter';
        
        
    }else if(selected == "trian") {
        isMouseActive = true;
        x1 = e.offsetX;
        y1 = e.offsetY;
        ctx.fillStyle = document.getElementById("color").value;
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineJoin = 'miter';
    }else if(selected == "circle") {
        isMouseActive = true;
        x1 = e.offsetX;
        y1 = e.offsetY;
        ctx.fillStyle = document.getElementById("color").value;
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("size").value;
        
    }else if(selected == "text") {
        isMouseActive = true;
        
        ctx.fillStyle = document.getElementById("color").value;
        
        font_str = document.getElementById("Font");
        ctx.font = document.getElementById("font_size").value+"px "+ font_str.options[font_str.selectedIndex].value;
        
        ctx.fillText( document.getElementById("text").value,e.offsetX, e.offsetY);
        
    }
        
});

canvas.addEventListener(moveEvent, function(e){
    if(selected == "brush") {
        if(!isMouseActive){
            return;
        }
            
        x2 = e.offsetX;
        y2 = e.offsetY;

        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();

        x1 = x2;
        y1 = y2;
    }else if(selected == "eraser") {
        if(!isMouseActive){
            return;
        }else {
            x4 = e.offsetX;
            y4 = e.offsetY;
            ctx.globalCompositeOperation = 'destination-out';
            ctx.beginPath();
            ctx.arc(x4, y4, document.getElementById("size").value/2, 0, 2 * Math.PI);
            ctx.fill();
            ctx.lineWidth = document.getElementById("size").value;
            ctx.beginPath();
            ctx.moveTo(x3, y3);
            ctx.lineTo(x4, y4);
            ctx.stroke();
            x3 = x4;
            y3 = y4;
            ctx.globalCompositeOperation = 'source-over';       
        }
    }else if(selected == "rect") {
        if(!isMouseActive){
            return;
        }else if(filled == true){
            
            ctx.putImageData(historyimage[index-1],0,0);
            
            ctx.beginPath();
            var w,h;
            w = e.offsetX - x1;
            h = e.offsetY - y1;
            ctx.rect(x1, y1, w, h);
            ctx.fill();
            
        }else {
            ctx.putImageData(historyimage[index-1],0,0);
            ctx.lineCap = 'square';
            ctx.beginPath();
            var w,h;
            w = e.offsetX - x1;
            h = e.offsetY - y1;
            ctx.rect(x1, y1, w, h);
            
            ctx.stroke();
            
        }
    }else if(selected == "trian") {
        if(!isMouseActive){
            return;
        }else if(filled == true) {
            var t_w, t_h;
            ctx.lineWidth = document.getElementById("size").value;
            ctx.lineCap = 'square';
            ctx.putImageData(historyimage[index-1],0,0);
            ctx.beginPath();
            ctx.moveTo(x1,y1);
            x2 = e.offsetX;
            y2 = e.offsetY;
            t_w = x2-x1;
            ctx.lineTo(x2,y2);
            ctx.lineTo(x1 - t_w, y2);
            ctx.fill();
        }else {
            var t_w, t_h;
            ctx.lineWidth = document.getElementById("size").value;
            ctx.putImageData(historyimage[index-1],0,0);
            ctx.beginPath();
            ctx.moveTo(x1,y1);
            x2 = e.offsetX;
            y2 = e.offsetY;
            t_w = x2-x1;
            ctx.lineTo(x2,y2);
            ctx.lineTo(x1 - t_w, y2);
            ctx.closePath();
            ctx.stroke();
        }
    }else if(selected == "circle") {
        if(!isMouseActive){
            return;
        }else if(filled == true) {
            ctx.putImageData(historyimage[index-1],0,0);
            ctx.beginPath();
            ctx.arc(x1,y1, Math.sqrt((e.offsetX-x1)*(e.offsetX-x1) + (e.offsetY - y1)*(e.offsetY - y1)),0,2*Math.PI);
            ctx.fill();
        }else {
            ctx.putImageData(historyimage[index-1],0,0);
            ctx.beginPath();
            ctx.arc(x1,y1, Math.sqrt((e.offsetX-x1)*(e.offsetX-x1) + (e.offsetY - y1)*(e.offsetY - y1)),0,2*Math.PI);
            ctx.stroke();
        }
    }
});

canvas.addEventListener(upEvent, function(e){
        isMouseActive  = false;
        
});

